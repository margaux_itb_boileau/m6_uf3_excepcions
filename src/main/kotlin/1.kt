fun main(){

}

fun divideixoCero(dividend:Int, divisor:Int):Int {
    var result:Int
    try {
        result = dividend/divisor
    }catch(e:ArithmeticException) {
        result = 0
    }
    return result
}