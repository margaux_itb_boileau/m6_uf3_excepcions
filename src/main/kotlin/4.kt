fun main() {
    val mutableList= mutableListOf<String>()

    try {

        mutableList.removeLast()
    }catch (e:NoSuchElementException) {
        println("Error: no s'ha trobat l'element")
    }catch (e: UnsupportedOperationException) {
        println("Error: operació no possible")
    }
}