fun main() {
}

fun aDoubleoU(input:Any):Double {
    try {
        return input.toString().toDouble()
    } catch (e:NumberFormatException) {
        return 1.0
    }
}