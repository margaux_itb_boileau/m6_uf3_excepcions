import java.io.File
import java.io.FileNotFoundException

fun main() {
    val file = File("./src/main/kotlin/nonexistent.txt")
    try {
        file.readText()
    } catch(e:FileNotFoundException) {
        println("El sistema no puede encontrar el archivo especificado")
    }
}